﻿// See https://aka.ms/new-console-template for more information

using OCPLibrary;

namespace ConsoleUI;

class Program
{
    public static void Main(string[] args)
    {
        List<PersonModel> applicants = new List<PersonModel>
        {
            new PersonModel {FirstName = "Dillon", LastName = "Cooper"},
            new PersonModel {FirstName = "Sue", LastName = "Storm"},
            new PersonModel {FirstName = "Nancy", LastName = "Roman"}
        };
        
        List<EmployeeModel> employees = new List<EmployeeModel>();
        Accounts accountProcessor = new Accounts();

        foreach (var person in applicants)
        {
            employees.Add(accountProcessor.Create(person));
        }
        
        foreach (var employee in employees)
        {
            Console.WriteLine($"{ employee.FirstName } { employee.LastName }: { employee.Email }");
        }

        Console.ReadLine();
    }
}
